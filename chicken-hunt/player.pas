
unit Player;

interface

uses
  SplashKit;

type
  TPlayerData = record
    player_sprite: sprite;
    score, bullets, player_time, time: integer;
  end;

function NewPlayer(): TPlayerData;
procedure DrawPlayer(player_to_draw: TPlayerData);
procedure UpdatePlayer(player_to_update: TPlayerData);
procedure HandleInput(var player: TPlayerData);

implementation

procedure load_sounds();
begin
  LoadSoundEffect('shot', 'shot_f.wav');
  LoadSoundEffect('empty', 'empty.wav');
  LoadSoundEffect('reload', 'reload_f.wav');
end;

function NewPlayer(): TPlayerData;
const
  STARTING_SCORE = 0;
  STARTING_BULLETS = 6;
  STARTING_TIME = 50;
var
  default_bitmap: bitmap;
begin
  load_sounds();

  default_bitmap := BitmapNamed('aim');

  result.player_sprite := CreateSprite(default_bitmap);

  result.bullets := STARTING_BULLETS;
  result.score := STARTING_SCORE;
  result.player_time := STARTING_TIME;
end;

procedure DrawPlayer(player_to_draw: TPlayerData);
begin
  DrawSprite(player_to_draw.player_sprite);
end;

procedure UpdatePlayer(player_to_update: TPlayerData);
begin
  UpdateSprite(player_to_update.player_sprite);
end;

procedure HandleInput(var player: TPlayerData);
var
  new_point: Point2D;
begin
  new_point := MousePosition();
  new_point.x := new_point.x - (SpriteWidth(player.player_sprite) / 2);
  new_point.y := new_point.y - (SpriteHeight(player.player_sprite) / 2);
  SpriteSetPosition(player.player_sprite, new_point);
  if MouseClicked(LEFT_BUTTON) then
  begin
    if player.bullets > 0 then
    begin
      PlaySoundEffect('shot');
      Dec(player.bullets);
    end else
      PlaySoundEffect('empty');
  end;
  if MouseClicked(RIGHT_BUTTON) then
  begin
    PlaySoundEffect('reload');
    player.bullets := 6;
  end;
end;

end.
