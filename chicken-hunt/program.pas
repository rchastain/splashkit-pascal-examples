
uses
  SplashKit, ChickenHunt, Player, Chicken;

var
  gm: TGameData;

begin
  OpenWindow('Chicken Hunt', 800, 800);
  LoadResourceBundle('game_bundle', 'chicken_hunt.txt');
  gm.player := NewPlayer();

  SetLength(gm.chickens, 1);
  gm.chickens[0] := NewChicken();

  while not QuitRequested() do
  begin
    ProcessEvents();
    HandleInput(gm.player);
    UpdateGame(gm);

    ClearScreen(ColorWhite);
    DrawGame(gm);

    RefreshScreen(60);
  end;
end.
