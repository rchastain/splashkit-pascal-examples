
unit ChickenHunt;

interface

uses
  SysUtils, SplashKit, Chicken, Player;

type
  TGameData = record
    player: TPlayerData;
    chickens: TChickenDataArray;
  end;

procedure UpdateTime(var player: TPlayerData);
procedure UpdateGame(var game: TGameData);
procedure DrawGame(game: TGameData);

procedure CheckChickenCollision(var game: TGameData);

implementation

procedure UpdateTime(var player: TPlayerData);
var
  count: integer;
begin
  count := CurrentTicks() div 1000;
  player.time := player.player_time - count;
  if player.time < 1 then
    player.time := 0;
end;

procedure UpdateGame(var game: TGameData);
const
  CHICKEN_OCCURRENCE = 0.006;
var
  i: integer;
begin
  for i := 0 to High(game.chickens) do
  begin
    UpdateChicken(game.chickens[i]);
  end;
  if Rnd() < CHICKEN_OCCURRENCE then
  begin
    SetLength(game.chickens, Succ(Length(game.chickens)));
    game.chickens[High(game.chickens)] := NewChicken();
  end;
  UpdatePlayer(game.player);
  CheckChickenCollision(game);
  UpdateTime(game.player);
end;

procedure DrawHUD(player: TPlayerData);
const
  shells: array[1..6] of integer = (720, 680, 640, 600, 560, 520);
var
  i: integer;
begin
  DrawText('SCORE: ' + IntToStr(player.score), ColorBlack, 10, 10, OptionToScreen());
  DrawText('TIME LEFT: ' + IntToStr(player.time), ColorBlack, 10, 20, OptionToScreen());
  for i := 1 to player.bullets do
    DrawBitmap('shell', shells[i], 690);
end;

procedure DrawGameOver(game: TGameData);
begin
  DrawText('GAME OVER', ColorRed, 'Bebedera.ttf', 40, ScreenWidth() / 2 - 125, ScreenHeight() / 2 - 80, OptionToScreen());
  DrawText('SCORE: ' + IntToStr(game.player.score), ColorRed, 'Bebedera.ttf', 35, ScreenWidth() / 2 - 125, ScreenHeight() / 2 - 20, OptionToScreen());
end;

procedure DrawGame(game: TGameData);
var
  background: bitmap;
  i: integer;
begin
  background := LoadBitmap('field', 'field.png');
  DrawBitmap(background, 0, 0, OptionToScreen());
  if game.player.time < 1 then
    DrawGameOver(game)
  else
  begin
    for i := 0 to High(game.chickens) do
      DrawChicken(game.chickens[i]);
  end;
  DrawHUD(game.player);
  DrawPlayer(game.player);
end;

procedure ApplyChickenCollision(chicken: TChickenData; var player: TPlayerData);
const
  S_CHICKEN_ADD_SCORE = 100;
  M_CHICKEN_ADD_SCORE = 50;
  L_CHICKEN_ADD_SCORE = 10;
begin
  case chicken.kind of
    S_CHICKEN: Inc(player.score, S_CHICKEN_ADD_SCORE);
    L_CHICKEN: Inc(player.score, L_CHICKEN_ADD_SCORE);
    M_CHICKEN: Inc(player.score, M_CHICKEN_ADD_SCORE);
  end;
end;

procedure CheckChickenCollision(var game: TGameData);
var
  i: integer;
begin
  i := 0;
  while i <= High(game.chickens) do
  begin
    if SpriteCollision(game.chickens[i].chicken_sprite, game.player.player_sprite) and MouseClicked(LEFT_BUTTON) and (game.player.bullets > 0) then
    begin
      SpriteSetDX(game.chickens[i].chicken_sprite, 0);
      ApplyChickenCollision(game.chickens[i], game.player);
      if i < High(game.chickens) then
      begin
        game.chickens[i] := game.chickens[High(game.chickens)];
      end;
      SetLength(game.chickens, Pred(Length(game.chickens)));
    end else
      Inc(i);
  end;
end;

end.
