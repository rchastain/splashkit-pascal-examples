
unit Chicken;

interface

uses
  SplashKit;

type
  TChickenKind =
  (
    S_CHICKEN,
    M_CHICKEN,
    L_CHICKEN
  );
  
  TChickenData = record
    chicken_sprite: sprite;
    kind: TChickenKind;
    chicken_speed: integer;
  end;

  TChickenDataArray = array of TChickenData;

function NewChicken(): TChickenData;
procedure DrawChicken(chicken_to_draw: TChickenData);
procedure UpdateChicken(chicken_to_update: TChickenData);

implementation

function NewChicken(): TChickenData;
var
  x, y: double;
begin
  result.kind := TChickenKind(Rnd(0, 3));

  if (result.kind = L_CHICKEN) then
  begin
    result.chicken_sprite := CreateSprite('ani_chicken', 'chicken');
    SpriteStartAnimation(result.chicken_sprite, 0);
    SpriteSetDX(result.chicken_sprite, Rnd() * 4 + 2);
  end
  else if (result.kind = S_CHICKEN) then
  begin
    result.chicken_sprite := CreateSprite('ani_chicken_s', 'chicken_s');
    SpriteStartAnimation(result.chicken_sprite, 0);
    SpriteSetDX(result.chicken_sprite, Rnd() * 4 + 2);
  end
  else
  begin
    result.chicken_sprite := CreateSprite('ani_chicken_l', 'chicken_l');
    SpriteStartAnimation(result.chicken_sprite, 0);
    SpriteSetDX(result.chicken_sprite, Rnd() * 4 + 2);
  end;

  x := Rnd(-20, -10);
  y := Rnd(0, 500);
  SpriteSetX(result.chicken_sprite, x);
  SpriteSetY(result.chicken_sprite, y);
end;

procedure DrawChicken(chicken_to_draw: TChickenData);
begin
  DrawSprite(chicken_to_draw.chicken_sprite);
end;

procedure UpdateChicken(chicken_to_update: TChickenData);
begin
  UpdateSprite(chicken_to_update.chicken_sprite);
end;

end.
