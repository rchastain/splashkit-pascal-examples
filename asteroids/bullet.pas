
unit Bullet;

interface

uses
  SysUtils, SplashKit, Utils, Player;

type
  TBulletData = record
    main_sprite: sprite;
    current_position: point2d;
    velocity: point2d;
    timer_name: string;
    bullet_timer: timer;
  end;

function NewBullet(const player: TPlayerData): TBulletData;
procedure UpdateBullet(var bullet_to_update: TBulletData);
procedure DrawBullet(const bullet_to_draw: TBulletData);

implementation

procedure create_timer(var this_bullet: TBulletData);
var
  number: integer;
  temp_name, timer_name: string;
  not_unique_name: boolean;
begin
  WriteLine('DEBUG create_timer');
  number := 0;
  temp_name := 'timer';
  not_unique_name := TRUE;
  while not_unique_name do
  begin
    Inc(number);
    timer_name := temp_name + IntToStr(number);
    if not HasTimer(timer_name) then
      not_unique_name := FALSE;
  end;
  this_bullet.timer_name := timer_name;
  this_bullet.bullet_timer := createtimer(timer_name);
  ResetTimer(this_bullet.bullet_timer);
  StartTimer(this_bullet.bullet_timer);
end;

function NewBullet(const player: TPlayerData): TBulletData;
const
  BULLET_MODIFIER = 30;
var
  rotation: double;
begin
  WriteLine('DEBUG NewBullet');
  rotation := SpriteRotation(player.main_sprite); // save players rotation
  result.main_sprite := CreateSprite(BitmapNamed('bullet'));
  result.current_position := player.current_position;

  // updating velocity based on existing player velocity and direction
  result.velocity.x := cos(rotation*M_PI/180) * BULLET_MODIFIER + player.velocity.x;
  result.velocity.y := sin(rotation*M_PI/180) * BULLET_MODIFIER + player.velocity.y;
  
  // set bullet to players location
  SpriteSetX(result.main_sprite, result.current_position.x);
  SpriteSetY(result.main_sprite, result.current_position.y);

  create_timer(result); // creating the cooldown timer
end;

procedure UpdateBullet(var bullet_to_update: TBulletData);
begin
  WriteLine('DEBUG UpdateBullet');
  bullet_to_update.current_position.x += bullet_to_update.velocity.x;
  bullet_to_update.current_position.y += bullet_to_update.velocity.y;

  // screen wrapping, if sprite is off screen place it on other side.
  screen_wrap(bullet_to_update.current_position, bullet_to_update.main_sprite);

  SpriteSetX(bullet_to_update.main_sprite, bullet_to_update.current_position.x);
  SpriteSetY(bullet_to_update.main_sprite, bullet_to_update.current_position.y);
end;

procedure DrawBullet(const bullet_to_draw: TBulletData);
begin
  DrawSprite(bullet_to_draw.main_sprite);   
end;

end.
