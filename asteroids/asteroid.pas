
unit Asteroid;

interface

uses
  SplashKit, Player, Utils;

type
  roid_style =
  (
    BLUE_SPIKEY,
    GREEN_SMOOTH,
    GREEN_SPIKEY,
    ORANGE_SMOOTH,
    PINK_SMOOTH,
    PURPLE_SMOOTH,
    RED_SMOOTH,
    YELLOW_SPIKEY
  );

  TRoidData = record
    main_sprite: sprite;
    roid_level: integer;
  end;
  
function NewRoid(x, y: double; roid_level: integer; scale: double; player: TPlayerData): TRoidData;
procedure DrawRoid(const roid_to_draw: TRoidData);
procedure UpdateRoid(var roid_to_update: TRoidData);

implementation

function roid_bitmap(style: roid_style): bitmap;
begin
  case style of
    BLUE_SPIKEY: result :=  BitmapNamed('blue_spikey');
    GREEN_SMOOTH: result :=  BitmapNamed('green_smooth');
    GREEN_SPIKEY: result :=  BitmapNamed('green_spikey');
    ORANGE_SMOOTH: result :=  BitmapNamed('orange_smooth');
    PINK_SMOOTH: result :=  BitmapNamed('pink_smooth');
    PURPLE_SMOOTH: result :=  BitmapNamed('purple_smooth');
    RED_SMOOTH: result :=  BitmapNamed('red_smooth');
    YELLOW_SPIKEY: result :=  BitmapNamed('yellow_spikey');
  end;
end;

function NewRoid(x, y: double; roid_level: integer; scale: double; player: TPlayerData): TRoidData;
var
  default_bitmap: bitmap;
begin
  WriteLine('DEBUG NewRoid');
  default_bitmap := roid_bitmap(roid_style(Rnd(8)));
  result.main_sprite := CreateSprite(default_bitmap);
  SpriteSetScale(result.main_sprite, scale);
  result.roid_level := roid_level;
  if scale = 1 then // fresh asteroid placement, reset player immunity
  begin
    ResetTimer(player.player_timer);
    StartTimer(player.player_timer);
    SpriteShowLayer(player.main_sprite, 2);
  end;
  SpriteSetRotation(result.main_sprite, Rnd(360));
  // Set asteroids initial x & y speed (.1 to 1)
  SpriteSetDX(result.main_sprite, 0.5);
  SpriteSetDY(result.main_sprite, 0.5);
  SpriteSetX(result.main_sprite, x);
  SpriteSetY(result.main_sprite, y);
  //WriteLine('DEBUG NewRoid line ' + {$I %LINE%});
end;

procedure DrawRoid(const roid_to_draw: TRoidData);
begin
  DrawSprite(roid_to_draw.main_sprite);
end;

procedure UpdateRoid(var roid_to_update: TRoidData);
var
  tmp_position: point2d;
begin
  tmp_position := spriteposition(roid_to_update.main_sprite); // get existing position
  screen_wrap(tmp_position, roid_to_update.main_sprite); // if sprite is off the screen place it on other side
  SpriteSetPosition(roid_to_update.main_sprite, tmp_position);
  UpdateSprite(roid_to_update.main_sprite); // Apply movement based on rotation and velocity in the sprite
end;

end.
