
(* Pascal version of SplashKit Asteroids: https://github.com/cab19/Asteroids *)

uses
  SplashKit, Asteroid, Bullet, Game;

const
  SCREEN_HEIGHT = 800;
  SCREEN_WIDTH = 1000;
  
var
  LGame: TGameData;

begin
  OpenWindow('ASTEROIDS', SCREEN_WIDTH, SCREEN_HEIGHT);
  LoadResourceBundle('game_bundle', 'asteroids.txt');
  
  LGame := NewGame();
  
  while not QuitRequested() do
  begin
    ProcessEvents();
    
    if LGame.menu_active then
    begin
      LoadMenu(LGame);
      LGame.menu_active := MenuInput(LGame); // if escape is pressed, returns FALSE and starts the game
    end else
    begin
      DrawGame(LGame);
      HandleInput(LGame);
      UpdateGame(LGame);
    end;
  end;
end.
