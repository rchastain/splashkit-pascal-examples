
unit Game;

interface

uses
  SysUtils, SplashKit, Utils, Asteroid, Bullet, Player;
  
type
  TBulletArray = array of TBulletData;
  TRoidArray = array of TRoidData;
  TGameData = record
    player: TPlayerData;
    bullets: TBulletArray;
    asteroids: TRoidArray;
    asteroid_count: integer;
    menu_active: boolean;
  end;

function NewGame(): TGameData;
procedure DrawGame(const game: TGameData);
procedure UpdateGame(var game: TGameData);
procedure HandleInput(var game: TGameData);
procedure LoadMenu(var game: TGameData);
function MenuInput(var game: TGameData): boolean;

implementation
  
procedure new_level(var game: TGameData); // add asteroids to game
var
  i: integer;
begin
  WriteLine('DEBUG new_level');
  //game.asteroids.clear(); // clear existing asteroids, only relevant when new game is started    
  //SetLength(game.asteroids, 0);
  SetLength(game.asteroids, game.asteroid_count);
  if game.asteroid_count > 0 then
  for i:= 0 to Pred(game.asteroid_count) do
  begin
    //game.asteroids.push_back(NewRoid(Rnd(screen_width()), Rnd(screen_height()), 3, 1, game.player));
    game.asteroids[i] := NewRoid(Rnd(ScreenWidth()), Rnd(ScreenHeight()), 3, 1, game.player);
  end;   
end;

function NewGame(): TGameData;
begin
  WriteLine('DEBUG NewGame');
  result.menu_active := TRUE; // initially menu is displayed
  result.player := NewPlayer();
  result.player.velocity.x := 0;
  result.player.velocity.y := 0;
  result.asteroid_count := 3;
  new_level(result);
end;

procedure draw_hud(const player: TPlayerData);
begin
  DrawText(IntToStr(player.score), ColorRed, fontnamed('atari'), 55, 55, 20, OptionToScreen());
  DrawText(IntToStr(player.lives), ColorRed, fontnamed('atari'), 55, ScreenWidth()-100, 20, OptionToScreen());
end;

procedure DrawGame(const game: TGameData);
var
  i: integer;
begin
  ClearScreen(ColorBlack);

  DrawBitmap(BitmapNamed('back'), 0, 0, OptionToScreen());

  DrawPlayer(game.player);
  
  if TimerTicks(game.player.player_timer) > IMMUNITY_TIME then
    SpriteHideLayer(game.player.main_sprite, 2);

  for i := 0 to High(game.bullets) do
    DrawBullet(game.bullets[i]);

  for i := 0 to High(game.asteroids) do
      DrawRoid(game.asteroids[i]);

  draw_hud(game.player);
  RefreshScreen(60);
end;

procedure handle_bullet_collision(var game: TGameData; bullet_idx, roid_idx: integer);
var
  tmp_timer: timer;
  roid_level, i: integer;
  scale: double;
begin
  FreeSprite(game.bullets[bullet_idx].main_sprite); // delete bullet sprite
  tmp_timer := timernamed(game.bullets[bullet_idx].timer_name); // reference to bullets timer
  FreeTimer(tmp_timer); // delete associated timer
  
  //game.bullets.erase(game.bullets.begin() + bullet_idx); // remove bullet from vector
  if bullet_idx < High(game.bullets) then
    game.bullets[bullet_idx] := game.bullets[High(game.bullets)];
  SetLength(game.bullets, Pred(Length(game.bullets)));
  
  // delete asteroid
  roid_level := game.asteroids[roid_idx].roid_level; // current level|size of the asteroid
  scale := 0;    

  // sort out scoring (and scaling if applicable)
  case roid_level of
    3: // large asteroid hit
    begin
      game.player.score += 20;
      game.player.free_life_score += 20;
      scale := 0.7;
    end;
    2: // medium asteroid hit
    begin
      game.player.score += 50;
      game.player.free_life_score += 50;
      scale := 0.4;
    end;
    1: // small asteroid hit
    begin
      game.player.score += 100;
      game.player.free_life_score += 100;
    end;
  end;

  if game.player.free_life_score > FREE_LIFE then
  begin
     game.player.free_life_score -=  FREE_LIFE; // negate the free life value
     Inc(game.player.lives); // add a life
  end;

  if scale > 0 then
  begin
      Dec(roid_level);
      SetLength(game.asteroids, Length(game.asteroids) + 2);
      for i := 0 to 1 do // spawn another two asteroids
          game.asteroids[High(game.asteroids) + i - 1] :=
              NewRoid(
                  SpriteX(game.asteroids[roid_idx].main_sprite), 
                  SpriteY(game.asteroids[roid_idx].main_sprite), 
                  roid_level, 
                  scale,
                  game.player
              );
  end;

  FreeSprite(game.asteroids[roid_idx].main_sprite); // delete asteroid sprite
  
  //game.asteroids.erase(game.asteroids.begin() + roid_idx); // delete asteroid from vector
   if roid_idx < High(game.asteroids) then
     game.asteroids[roid_idx] := game.asteroids[High(game.asteroids)];
  SetLength(game.asteroids, Pred(Length(game.asteroids)));
  
  if Length(game.asteroids) = 0 then
  begin
    Inc(game.asteroid_count); // increment amount of asteroids per level
    new_level(game);
  end;
end;

procedure check_collisions(var game: TGameData);
var
  i, j: integer;
  player_spawned: boolean;
begin
  for i := 0 to High(game.asteroids) do
  begin
    // check for collision between player and current asteroid
    if SpriteCollision(game.player.main_sprite, game.asteroids[i].main_sprite) then
    begin
      if TimerTicks(game.player.player_timer) > IMMUNITY_TIME then
      begin
        PlaySoundEffect(SoundEffectNamed('explode'));
        FreeSprite(game.player.main_sprite);
        Dec(game.player.lives);              
        if game.player.lives > 0 then
        begin // player still has a life
          ResetTimer(game.player.player_timer); // reset timer for spawn delay
          StartTimer(game.player.player_timer);
          player_spawned := FALSE;
          while not player_spawned do // stop game for player visual feedback
            if TimerTicks(game.player.player_timer) > SPAWN_DELAY then
            begin
              SpawnPlayer(game.player,TRUE); // spawn new player centred in screen
              player_spawned := TRUE;
            end;

        end else
        begin // no lives, end game, show menu
          StopMusic();
          game.menu_active := TRUE; // show menu
        end;
      end else
      begin
        ResetTimer(game.player.player_timer); // reset timer for immunity
        StartTimer(game.player.player_timer); // keeps player immune for a while longer
      end;
    end else
    begin // check for collision between bullets and asteroids
      for j := 0 to High(game.bullets) do
      begin
        // check current bullet and current asteroid for collision
        if SpriteCollision(game.bullets[j].main_sprite, game.asteroids[i].main_sprite) then
        begin
          PlaySoundEffect(SoundEffectNamed('explode')); // play sound effect
          handle_bullet_collision(game, j, i); // process the collision
        end;
      end;
    end;
  end;
end;

procedure UpdateGame(var game: TGameData);
const
  BULLET_LIFE = 3000;
var
  i: integer;
  tmp_timer: timer;
begin
  UpdatePlayer(game.player); 

  //Update bullets, check bullet life, if expired delete sprite, timer and entry in vector.
  i := 0;
  while i <= High(game.bullets) do
  begin    
    if TimerTicks(game.bullets[i].bullet_timer) > BULLET_LIFE then
    begin // bullet expired
      FreeSprite(game.bullets[i].main_sprite); // remove sprite
      tmp_timer := timernamed(game.bullets[i].timer_name); // reference to bullets timer
      FreeTimer(tmp_timer); // delete timer
      //game.bullets.erase(game.bullets.begin() + i); // remove from vector
      if i < High(game.bullets) then
        game.bullets[i] := game.bullets[High(game.bullets)];
      SetLength(game.bullets, Pred(Length(game.bullets)));
    end else
    begin        
      UpdateBullet(game.bullets[i]); // bullet still active update it
      Inc(i);
    end;
  end;
  
  for i := 0 to High(game.asteroids) do
  begin
    UpdateRoid(game.asteroids[i]);
  end;

  check_collisions(game);
end;

procedure HandleInput(var game: TGameData);
var
  rotation: double;
begin
  rotation := SpriteRotation(game.player.main_sprite);
  
  if KeyDown(A_KEY) or KeyDown(LEFT_KEY) then SpriteSetRotation(game.player.main_sprite, rotation - PLAYER_ROTATE_SPEED);
  if KeyDown(D_KEY) or KeyDown(RIGHT_KEY) then SpriteSetRotation(game.player.main_sprite, rotation + PLAYER_ROTATE_SPEED);

  SpriteHideLayer(game.player.main_sprite, 1); // sprite layer representing thrust
  
  if KeyDown(W_KEY) or KeyDown(UP_KEY) then
  begin // add to velocity depending on direction of thrust and existing velocity
    game.player.velocity.x += cos(rotation*M_PI/180) * PLAYER_SPEED;
    game.player.velocity.y += sin(rotation*M_PI/180) * PLAYER_SPEED;
    spriteshowlayer(game.player.main_sprite, 1); // show thrust layer
    PlaySoundEffect(SoundEffectNamed('thrust')); // play thrust sound effect
  end;

  if KeyTyped(S_KEY) then
  begin
    FreeSprite(game.player.main_sprite); // remove players sprite
    SpawnPlayer(game.player,FALSE); // spawn new sprite randomly on screen
  end;

  if KeyDown(SPACE_KEY) and (TimerTicks(game.player.shoot_cooldown) > COOLDOWN_TIME) then
  begin // shooting cooldown, prevents spam...
    ResetTimer(game.player.shoot_cooldown); // reset cooldown timer
    StartTimer(game.player.shoot_cooldown); // start cooldown timer
    
    //game.bullets.push_back(NewBullet(game.player)); // create new bullet
    SetLength(game.bullets, Succ(Length(game.bullets)));
    game.bullets[High(game.bullets)] := NewBullet(game.player);
    
    PlaySoundEffect(SoundEffectNamed('shoot')); // play sound effect
  end;
end;

procedure LoadMenu(var game: TGameData);
var
  i: integer;
  menu: bitmap;
begin
  //WriteLine('DEBUG LoadMenu');
  ClearScreen(ColorBlack); // clear screen
  DrawBitmap(BitmapNamed('back'), 0, 0, OptionToScreen()); // draw background image
  
  for i := 0 to High(game.asteroids) do
  begin
    UpdateRoid(game.asteroids[i]);
    DrawRoid(game.asteroids[i]);
  end;

  draw_hud(game.player);
  menu := BitmapNamed('menu');
  DrawBitmap(menu, ScreenWidth() div 2 - BitmapWidth(menu) div 2, ScreenHeight() div 2 - BitmapHeight(menu) div 2, OptionToScreen());
  RefreshScreen(60);
end;

function MenuInput(var game: TGameData): boolean;
begin
  if KeyDown(ESCAPE_KEY) then
  begin
    WriteLine('START GAME');
    PlayMusic(loadmusic('background', 'background.mp3'), -1);
    game := NewGame();
    exit(FALSE);
  end else
    exit(TRUE);
end;

end.
