
unit Player;

interface

uses
  SplashKit, Utils;
  
const
  PLAYER_SPEED = 0.15;
  PLAYER_ROTATE_SPEED = 5;
  PLAYER_LIVES = 4;
  FREE_LIFE = 5000;
  COOLDOWN_TIME = 400;
  IMMUNITY_TIME = 3000;
  SPAWN_DELAY = 2500;

type
  TPlayerData = record
    main_sprite: sprite;
    score: integer;
    free_life_score: integer;
    lives: integer;
    current_position: point2d;
    velocity: point2d;
    shoot_cooldown: timer;
    player_timer: timer;   
  end;
  
function NewPlayer(): TPlayerData;
procedure SpawnPlayer(var player: TPlayerData; centred: boolean);
procedure DrawPlayer(const player_to_draw: TPlayerData);
procedure UpdatePlayer(var player_to_update: TPlayerData);

implementation

procedure SpawnPlayer(var player: TPlayerData; centred: boolean);
begin
  player.main_sprite := CreateSprite(BitmapNamed('ship'));   // default ship image
  SpriteAddLayer(player.main_sprite, BitmapNamed('shipt'), 'shipt'); // ship with thrust
  SpriteAddLayer(player.main_sprite, BitmapNamed('shipi'), 'shipi'); // ship halo reflecting immunity
  SpriteSetRotation(player.main_sprite, -90); // point ship north
  player.velocity.x := 0;
  player.velocity.y := 0;
  if centred then // new players are spawned in the centre of the screen
  begin
    player.current_position.x := (ScreenWidth() - SpriteWidth(player.main_sprite)) div 2;
    player.current_position.y := (ScreenHeight() - SpriteHeight(player.main_sprite)) div 2;
    ResetTimer(player.player_timer); // reset timer for immunity
    StartTimer(player.player_timer);
    SpriteShowLayer(player.main_sprite, 2); // show halo immunity layer
  end else
  begin // hyperspace used, place the ship randomly without immunity
    player.current_position.x := Rnd(ScreenWidth());
    player.current_position.y := Rnd(ScreenHeight());
  end;
  SpriteSetX(player.main_sprite, player.current_position.x);
  SpriteSetY(player.main_sprite, player.current_position.y);
end;

function NewPlayer(): TPlayerData;
begin
  result.shoot_cooldown := CreateTimer('shoot_cooldown');
  ResetTimer(result.shoot_cooldown);
  StartTimer(result.shoot_cooldown);

  result.player_timer := CreateTimer('player_timer');
  ResetTimer(result.player_timer);
  StartTimer(result.player_timer);

  SpawnPlayer(result, TRUE); // create player sprite and place it on the screen

  result.lives := PLAYER_LIVES;
  result.score := 0;
  result.free_life_score := 0;
end;

procedure DrawPlayer(const player_to_draw: TPlayerData);
begin
  DrawSprite(player_to_draw.main_sprite);
end;

procedure UpdatePlayer(var player_to_update: TPlayerData);
begin
  player_to_update.current_position.x += player_to_update.velocity.x;
  player_to_update.current_position.y += player_to_update.velocity.y;

  screen_wrap(player_to_update.current_position, player_to_update.main_sprite);

  SpriteSetX(player_to_update.main_sprite, player_to_update.current_position.x);
  SpriteSetY(player_to_update.main_sprite, player_to_update.current_position.y);
end;

end.
