
local LWords = {
  'BitmapHeight',
  'BitmapNamed',
  'BitmapWidth',
  'ClearScreen',
  'ColorBlack',
  'ColorRed',
  'CreateSprite',
  'DrawBitmap',
  'DrawSprite',
  'DrawSprite',
  'DrawText',
  'FreeSprite',
  'FreeTimer',
  'HasTimer',
  'KeyDown',
  'KeyTyped',
  'LoadResourceBundle',
  'OpenWindow',
  'OptionToScreen',
  'PlayMusic',
  'PlaySoundEffect',
  'ProcessEvents',
  'QuitRequested',
  'RefreshScreen',
  'ResetTimer',
  'Rnd',
  'ScreenHeight',
  'ScreenWidth',
  'SoundEffectNamed',
  'SpriteAddLayer',
  'SpriteCollision',
  'SpriteHeight',
  'SpriteHideLayer',
  'SpriteRotation',
  'SpriteRotation',
  'SpriteSetDX',
  'SpriteSetDY',
  'SpriteSetPosition',
  'SpriteSetRotation',
  'SpriteSetX',
  'SpriteSetY',
  'SpriteWidth',
  'SpriteX',
  'SpriteY',
  'StartTimer',
  'StopMusic',
  'TimerTicks',
  'UpdateSprite',
  'WriteLine'
}

function NoCase(s)
  s = string.gsub(
    s,
    '%a',
    function(c) return string.format('[%s%s]', string.lower(c), string.upper(c)) end
  )
  return s
end

function System(ACommand)
  local i, result, popen = 0, {}, io.popen
  for s in popen(ACommand):lines() do
    i = i + 1
    result[i] = s
  end
  return result
end

io.write(#LWords .. ' words loaded\n')

local LFileList = System('ls -1 *.pas')

io.write(#LFileList .. ' files found\n')

for i = 1, #LFileList do
  io.write('Processing ' .. LFileList[i] .. '\n')
  
  local LFile, LErr = io.open(LFileList[i], 'r')
  
  if LFile then
    local LText = LFile:read('*a')
    io.close(LFile)
    
    for j = 1, #LWords do
      LText = string.gsub(LText, NoCase(LWords[j]), LWords[j])
    end
    
    LFile, LErr = io.open(LFileList[i], 'w')
    if LFile then
      LFile:write(LText)
      io.close(LFile)
    else
      io.write(LErr)
    end
  else
    io.write(LErr)
  end
end

io.write('Done\n')
