
unit Utils;

interface

uses
  SplashKit;

const
  M_PI = PI;

procedure screen_wrap(var target_object: point2d; s: sprite);

implementation

procedure screen_wrap(var target_object: point2d; s: sprite);
begin
  if target_object.x > ScreenWidth() then
    target_object.x := 0;
  if target_object.x < SpriteWidth(s) * -1 then
    target_object.x := ScreenWidth();
  if target_object.y > ScreenHeight() then
    target_object.y := 0;
  if target_object.y < SpriteHeight(s) * -1 then
    target_object.y := ScreenHeight();
end;

end.
