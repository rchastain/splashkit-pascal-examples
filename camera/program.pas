
(* https://splashkit.io/articles/guides/tags/camera/about-camera/ *)

uses
  SplashKit;

const
  SCREEN_BORDER = 100;

procedure UpdateCameraPosition(player_x, player_y: double);
var
  left_edge, right_edge, top_edge, bottom_edge: double;
begin
  // Test edge of screen boundaries to adjust the camera
  left_edge := cameraX() + SCREEN_BORDER;
  right_edge := left_edge + screenWidth() - 2 * SCREEN_BORDER;
  top_edge := cameraY() + SCREEN_BORDER;
  bottom_edge := top_edge + screenHeight() - 2 * SCREEN_BORDER;

  // Test if the player is outside the area and move the camera
  // the player will appear to stay still and everything else
  // will appear to move :)

  // Test top/bottom of screen
  if (player_y < top_edge) then
  begin
    MoveCameraBy(0, player_y - top_edge);
  end
  else if (player_y > bottom_edge) then
  begin
    MoveCameraBy(0, player_y - bottom_edge);
  end;

  // Test left/right of screen
  if (player_x < left_edge) then
  begin
    MoveCameraBy(player_x - left_edge, 0);
  end
  else if (player_x > right_edge) then
  begin
    MoveCameraBy(player_x - right_edge, 0);
  end;
end;

var
  player_x, player_y: double;
  
begin
  OpenWindow('Camera Test', 800, 800);

  player_x := 400;
  player_y := 400;

  while not QuitRequested() do
  begin
    // Handle input to adjust player movement
    ProcessEvents();
    
    if KeyDown(LEFT_KEY)  then player_x -= 3;
    if KeyDown(RIGHT_KEY) then player_x += 3;
    if KeyDown(DOWN_KEY)  then player_y += 3;
    if KeyDown(UP_KEY)    then player_y -= 3;

    UpdateCameraPosition(player_x, player_y);

    // Redraw everything
    ClearScreen(ColorBlack);

    // Draw to the screen
    DrawText('HUD - top left', ColorWhite, 0, 0, optionToScreen());

    // as well as the player who can move
    FillCircle(ColorYellow, player_x, player_y, 20);

    // including something stationary - it doesn't move
    FillRectangle(ColorWhite, 400, 200, 10, 10);

    RefreshScreen(60);
  end;

end.
