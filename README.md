# SplashKit Pascal examples

Pascal examples for the [SplashKit](https://splashkit.io/) library.

Adaptation of C++ examples or projects.

## Contents

### Tutorials

  * [Animation](animation)
  * [Camera](camera)
  * [Drawing](drawing)

### Games

  * [Asteroids](asteroids)
  * [Chicken Hunt](chicken-hunt)
