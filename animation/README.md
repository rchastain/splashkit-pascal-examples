# Animation demo

Pascal adaptation of this [example](https://splashkit.io/articles/guides/tags/animations/animation/).

## Screenshot

![screenshot](screenshot.png)

## Build

    skm fpc program

## Credits

  * https://opengameart.org/content/frog-player-spritesheets
  * https://opengameart.org/content/512-sound-effects-8-bit-style
