
(* https://splashkit.io/articles/guides/tags/animations/animation/ *)

uses
  SplashKit;

const
  CScript = 'frog.txt';
  CSpriteSheet = 'PlayerSprite0.png'; (* https://opengameart.org/content/frog-player-spritesheets *)
  
var
  LFrog: bitmap;
  LScript: animationscript;
  LAnimation: animation;
  LDrawingOpt: drawingoptions;
  
begin
  OpenWindow('Animation test', 192, 192);
  Delay(100);
  ClearScreen(ColorWhite);
  RefreshScreen();

  // We could load all of the resources in a bundle
  // load_resource_bundle("dance bundle", "dance_bundle.txt");
  //
  // Then access by name
  // animation_script LScript = animation_script_named("WalkingScript");
  // bitmap LFrog = bitmap_named("FrogBmp");

  // Loading them separately

  // Load image and set its cell details
  LFrog := LoadBitmap('FrogBmp', CSpriteSheet);
  
  BitmapSetCellDetails(LFrog, 64, 64, 4, 2, 8); // cell width, height, cols, rows, count

  // Load the animation script
  LScript := LoadAnimationScript('WalkingScript', CScript);
  // Create the animation
  LAnimation := CreateAnimation(LScript, 'Wink');
  // Create a drawing option
  LDrawingOpt := OptionWithAnimation(LAnimation);

  // Basic event loop
  while not QuitRequested() do
  begin
    // Draw the bitmap - using opt to link to animation
    ClearScreen(ColorWhite);
    
    DrawText('Jump J', ColorBlack, 8, 8, OptionToScreen());
    DrawText('Wink W', ColorBlack, 8, 24, OptionToScreen());
    DrawText('Quit ESC', ColorBlack, 8, 40, OptionToScreen());
    
    DrawBitmap(LFrog, 64, 64, LDrawingOpt);
    RefreshScreen(60);

    // Update the animation
    UpdateAnimation(LAnimation);
    ProcessEvents();

    // Switch animations
    if KeyTyped(W_KEY) then
      AssignAnimation(LAnimation, 'Wink')
    else if KeyTyped(J_KEY) then
      AssignAnimation(LAnimation, 'Jump')
    else if KeyTyped(ESCAPE_KEY) then
      Break;
  end;
end.
