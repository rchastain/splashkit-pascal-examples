
(* https://splashkit.io/articles/guides/tags/starter/get-started-drawing/ *)

uses
  SplashKit;
  
begin
  OpenWindow('Animation test', 800, 600);
  Delay(100);
  ClearScreen(ColorWhite);
  FillEllipse(ColorBrightGreen, 0, 400, 800, 400);
  FillRectangle(ColorGray, 300, 300, 200, 200);
  FillTriangle(ColorRed, 250, 300, 400, 150, 550, 300);
  RefreshScreen();
  Delay(5000);
end.
